# Planung
[>>> Aktuelle Planung (PDF) <<<](./planung/dismod1819_planung_repetitorium.pdf)

Das Repetitorium findet vom 18.03. bis 22.03. im **Magnus-Hörsaal** statt.

Nach Bedarf und Interesse können am 25.03. und 26.03. **weitere Treffen** stattfinden. Ab 27.03. steht das **Lernzentrum** zur fachlichen Unterstützung bereit (Mo. bis Fr., jeweils 12 bis 15 Uhr).

Im Repetitorium werden die Grundlagen der **Aussagenlogik, Graphen, Markov-Ketten, Reguläre Sprachen** und **Kontextfreien Grammatiken** wiederholt.

Die Unterlagen zur Diskreten Modellierung (Skript, Folien, Übungsblätter, Altklausuren etc.) sind alle auf der
[Dismod-Kursseite](http://www.thi.informatik.uni-frankfurt.de/lehre/dismod/ws1819/#material) zu finden. Zur Klausurvorbereitung ist insbesondere
das Dokument **Allgemeine Hinweise zur Klausur** zu beachten!

# Voraussetzungen
Es wir davon ausgegangen, dass die Studenten vertraut sind mit:

- Mengen und Mengenoperationen
- kartesisches Produkt, Paare, Tupel
- Relationen und Funktionen

# Aufgaben
Nach der allgemeinen Besprechung der Grundlagen soll das Wissen zur Lösung von Aufgaben angewendet werden. Bei den Aufgaben wird es sich überwiegend um (ggf. modifizierte) Aufgaben aus dem **Skript** handeln. Die geplanten Aufgaben und weitere Aufgaben zur Klausurvorbereitung werden in diesem Dokument gesammelt:

[>>> Sammlung Aufgaben (PDF) <<<](./aufgaben/dismod1819_aufgaben_uebersicht.pdf)

Sofern Aufgaben aus dem Skript modifiziert wurden, sind die modifizierten Aufgaben hier zu finden:

[>>> Modifizierte Skript-Aufgaben (PDF) <<<](./aufgaben/dismod1819_aufgaben_modifiziert.pdf)

Es wird empfohlen sich die Aufgaben _vor_ Besuch des Repetitoriums anzuschauen.

# Kontakt
Fragen, Anregungen, Kritik bitte an **Karl** schicken: <karl.fehrs@stud.uni-frankfurt.de>

Feedback kann in **anonymer Form** auch hier abgegeben werden: [Feedback](https://medienpad.de/p/feedback_repetitorium_dismod_ws1819)